use std::env;
use std::path::Path;
use std::fs;
use std::fs::*;

use std::thread;
use std::sync::{Arc, Mutex};
use std::collections::vec_deque::VecDeque;

extern crate num_cpus;

fn main() {
    let args: Vec<String> = env::args().collect();
    let prompt = 
    "usage: sortfiles [path]\n\n\
    Sorts *.jpg and *.mp3 files in directory [path] in an endless loop. \
    Creates [path]/jpg and [path]/mp3 subdirectories. \
    Everything else remains in place."
    ;
    match args.len() {
        1 => println!("{}", prompt),
        2 => {
            run(&args[1].parse::<String>().unwrap());
        },
        _ => println!("{}{}", "Error: too many arguments, aborting\n", prompt),
    }
}

fn run(path: &str) {
    let mp3_path_str = format!("{}{}", path, "/mp3");
    let mp3_path = Path::new(&mp3_path_str);
    
    let jpg_path_str = format!("{}{}", path, "/jpg");
    let jpg_path = Path::new(&jpg_path_str);
    
    let path = Path::new(path);
    
    if !path.is_dir() {
        panic!("Error: argument is not a directory, aborting.");
    }
    
    let mut has_mp3_folder = false;
    let mut has_jpg_folder = false;
    for entry in path.read_dir().expect("Error: read_dir failed, aborting") {
        // probably too harsh:
        let entry = entry.expect("Error: traversing directory entries failed, aborting");
        // if fails to read metadata ignore and continue
        if let Ok(metadata) = fs::metadata(entry.path()) {
            if metadata.is_dir() {
                if let Some(file_name_str) = entry.file_name().to_str() {
                    match file_name_str {
                        "mp3" => has_mp3_folder = true,
                        "jpg" => has_jpg_folder = true,
                        _ => (),
                    }
                }
            }
        }
    }
    
    if !has_mp3_folder {
        create_dir(mp3_path)
        .expect("Error: failed to create \"[path]/mp3\" directory, aborting");
    }
    if !has_jpg_folder {
        create_dir(jpg_path)
        .expect("Error: failed to create \"[path]/jpg\" directory, aborting");
    }
    
    let mp3s = Arc::new(Mutex::new(VecDeque::<NamedFileBytes>::new()));
    let jpgs = Arc::new(Mutex::new(VecDeque::<NamedFileBytes>::new()));
    
    {
        let mp3s = mp3s.clone();
        let jpgs = jpgs.clone();
        thread::spawn(move || reader(mp3s, jpgs) );
    }
    // N + 1
    for _ in 0..num_cpus::get() {
        let mp3s = mp3s.clone();
        let jpgs = jpgs.clone();
        thread::spawn(move || writer(mp3s, jpgs) );
    }
    loop {
        thread::sleep(std::time::Duration::from_secs_f64(1e15f64));
    }
}

fn reader(
    mp3s: Arc<Mutex<VecDeque<NamedFileBytes>>>,
    jpgs: Arc<Mutex<VecDeque<NamedFileBytes>>>,
) {
    let path = env::args().nth(1).unwrap();
    let path = Path::new(&path);
    loop {
        for entry in path.read_dir().expect("Error: read_dir failed, aborting") {
            let entry = entry.expect("Error: traversing directory entries failed, aborting");
            if let Ok(metadata) = fs::metadata(entry.path()) {
                if let Some(file_name) = entry.file_name().to_str() {
                    if metadata.is_file()
                    && !metadata.permissions().readonly() 
                    && (file_name.ends_with(".mp3") || file_name.ends_with(".jpg")) {
                        let file_bytes = read(entry.path())
                        .expect("Error: failed to read file, aborting");
                        // i don't quite understand why shouldn't we async write immediately,
                        // since writing in batches would require some real effort
                        // and almost certainly isn't cross-platform
                        // 
                        // otherwise anything we do is probably pointless
                        // since calling system functions should be costly, i assume
                        // and after calling them our writes would probably get rescheduled,
                        // so adding another system ontop of OS ones would be redundant 
                        // when we have no real control
                        // over how kernel schedules file IO
                        // 
                        // but it requires experimental check
                        
                        // and i don't understand using one thread for reading either
                        // why would it keep up with multiple writers?
                        // wouldn't it just make it N times slower?
                        if file_name.ends_with(".mp3") {
                            mp3s.lock().unwrap()
                            .push_back(NamedFileBytes{
                                name: file_name.into(), 
                                bytes: file_bytes
                            });
                        }
                        else if file_name.ends_with(".jpg") {
                            jpgs.lock().unwrap()
                            .push_back(NamedFileBytes{
                                name: file_name.into(), 
                                bytes: file_bytes
                            });
                        }
                        // simply overwrite files if they already exist?
                        
                        // imagine user overwriting a file in this section
                        // then it means we would be deleting a new file
                        // which is incorrect
                        // 
                        // (if user simply deletes a file our program halts with error)
                        // 
                        // in extreme case we'd like to
                        // either read+delete "atomically"(broadly speaking)
                        // or check incompletely + delete "atomically"
                        // 
                        // seems it's better to just block user writes to the directory?
                        // which sounds insane as well
                        
                        // let's just ignore this case cause it should happen once in a blue moon

                        // as rust docs say
                        // no guarantee to delete immediately(thanks to filedescriptors, for example)
                        remove_file(entry.path())
                        .expect("Error: failed to remove file, aborting");
                    }
                }
            }
        }
    }
}

fn writer(
    mp3s: Arc<Mutex<VecDeque<NamedFileBytes>>>,
    jpgs: Arc<Mutex<VecDeque<NamedFileBytes>>>,
) {
    // boilerplate, cause rust is killing me
    let path = env::args().nth(1).unwrap();
    let mp3_path_str = format!("{}{}", path, "/mp3");
    
    let jpg_path_str = format!("{}{}", path, "/jpg");
    loop {
        if let Some(e) = mp3s.lock().unwrap().pop_back() {
            let write_path = format!("{}/{}", mp3_path_str, e.name);
            write(write_path, e.bytes).expect("Error: failed to write file, aborting");
        }
        
        if let Some(e) = jpgs.lock().unwrap().pop_back() {
            let write_path = format!("{}/{}", jpg_path_str, e.name);
            write(write_path, e.bytes).expect("Error: failed to write file, aborting");
        }
    }
}

#[derive(Clone)]
struct NamedFileBytes {
    name: String,
    bytes: Vec<u8>,
}