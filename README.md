# trivial filesystem io (std::fs) in Rust, threaded

# USE AT YOUR OWN RISK

## (lab assignment)

# usage: sortfiles [path]
    
    Sorts *.jpg and *.mp3 files in directory [path] in a sad threaded loop.
    Creates [path]/jpg and [path]/mp3 subdirectories.
    Everything else remains in place.